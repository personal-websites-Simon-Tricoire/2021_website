# Personal website (2020-2021)

This a website I made in 2020 as a way to show my projects, my experiences and my skills
to possible employers. I used it during two years.

I did all the design myself using Photoshop and Adobe xD for the mock-up.
I also did all the programmation by myself learning a lot about Html, Css (especially flexbox).

One of my main concerns programming it, was to make it as ecological as I can, inspired by
[Low tech magazine](https://solar.lowtechmagazine.com/).

I used in particular [WebsiteCarbon](https://www.websitecarbon.com/) to analyse the
impact of my website on the planet.


<figure>
  <img src="Design/Website_1.png" width="500">
</figure>

<br>

<figure>
  <img src="Design/Website_3.png" width="500">
</figure>

<br>

<figure>
  <img src="Design/Website_2.png" width="500">
</figure>
